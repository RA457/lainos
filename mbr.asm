; This is the MASTER BOOT RECORD file which starts everything

[bits 16] ; This is a derivative so sort of like a macro which tells the system to switch to 16 bit
; we are starting with 16 bit cause the BIOS will jump to bootloader when the CPU is still in 16 bit mode
[org 0x7c00] ; this is also a derivative which will set the counter to the given location
; This is important cause we specify the memory address where the BIOS is placing the boot loader and on x86 machine this is the loaction which first sector of boot device which contains the boot loader code is copied into phyical memory

KERNEL_OFFSET equ 0x1000 ; location in memory to load kernel into

;So BIOS stores the boot drive in dl register we are freeing it up for future use of the register
mov [BOOT_DRIVE], dl

; set up stack
mov bp, 0x9000 ; bottom of the stack
mov sp, bp ; sp is the top of the stack the stack grows downward
; we assign it to 0x9000 to make sure we are far away from other bootloader related memory to avoid collision
; this stack will be used by the call and ret statements to keep track of memory address when executing assembly procedures

;this are in another file
call load_kernel ; basically instructs BIOS to load kernel from disk into memory at the KERNEL_OFFSET
;this uses disk_load procedure which is in another file

; This procedure will take three parameters
; the memory location to plcae to read data into i guess (bx)
; the number of sectors to read (dh)
; the disk to read from (dl)

call switch_to_32bit ; as the name suggest

jmp $ ; jmp to the same line so like creating a loop as $ points to the begining of the address of the expression

; the files needed
%include "disk.asm"
%include "gdt.asm"
%include "switchTo32bit.asm"

[bits 16]
; this will take three paramets as showed above
load_kernel:
	mov bx, KERNEL_OFFDET ; bx is loaded with position to load shit in
	mov dh, 2 ; dh will store the number of sectors
	mov dl, [BOOT_DRIVE] ; dl will be the disk addr

[bits 32]
BEGIN_32BIT:
	call KERNEL_OFFSET ; give control to the kernel
	jmp $ ; create a loop for when the kernel returns after excution

;boot drive variable
BOOT_DRIVE db 0

;padding the remaing data with 0 cause non empty allocation gives error i think
times 510 - ($-$$) db 0 ; $ refes to the address of the current line where $$ stands for the location of whole section of segmnet

; this is so called magic number which validates the MBR and make sures BIOS only loads this shit and not something else
;magic number
dw 0xaa55

