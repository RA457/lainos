
[org 0x7c00] ; Tell the assembler where the code will be loaded

	;; Set video mode
	mov ah, 0x00 ; ah 0x00 = set video mode
	mov al, 0x03 ; 80x25 text mode
	int 0x10

	;; Change color
	mov ah, 0x0B
	mov bh, 0x00
	mov bl, 0x01 ; Color Blue
	int 0x10

	mov ah, 0x0e ; Tty

	mov bx, HELLO_MSG ; string is stored in ax and bx holds its address ig so we will pass it as the parameter
	call print_str

	mov bx, GOODBYE_MSG
	call print_str

	mov dx, 0x07CE ; Sample hex number
	call print_hex


jmp $ ; Jump to current address = Infinite loop

%include "utils/print_function.asm"

; Data
HELLO_MSG:
	db "Lain OS", 13, 10, 0 

GOODBYE_MSG:
	db '', 0

	; Padding numbers for firs 512
	times 510-($-$$) db 0
	dw 0xaa55 ; Last two bytes for the magic number
