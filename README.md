# lainOS

Trying to make a really simple "OS". Its just a bootloader right now. I am still learning stuff.

![Image of the OS](sample.png)

#### SETUP AND EXECUTE:-
1. Install nasm.
2. Install qemu for convinience.
3. Compile with nasm main.asm -f bin -o main.bin
4. Run with qemu main.bin.
