print_str:
	pusha ; Push all register values to the stack
	mov ah, 0x0e ; Tty
	mov al, [bx] ; bx is the pointer for this stream.
	psloop:
		int 0x10 ; prints al
		inc bx ; inc the pointer
		mov al, [bx]
		cmp al, 0 ; check for the stirng end
		jne psloop ; repeat
	popa
	ret

print_hex:
	;; Hex 0-9 A-f
	;; Ascii '0' - '9' = hex 0x30-0x39
	;; Ascii 'A' - 'F' = hex 0x41-0x46
	;; Ascii 'a' - 'f' = hex 0x61-0x66
	pusha ; Push all register values to the stack
	
	mov cx, 0 ; Initialize loop counter for the number of digits in the hex
	hex_loop:
		cmp cx, 4 ; End of loop check
		je end_hexLoop
		
		;; Convert dx to ascii
		mov ax, dx ; save dx for future
		and ax, 0x000F ; Anding it with 0000 0000 0000 1111 so only the last value is alive
		; So we do 0001 0010 1010, 1011 with 0000 0000 0000 1111
		add al, 0x30 ; get to the ascii number of letter value
		cmp al, 0x39 ; ax: 4sec: 2sec ah + 2sec al. Is hex value greater than 39 which will fall under 'A' - 'H' category
		jle move_intoBX ; Less than equal to
		add al, 0x7 ; To get ascii 'A' - 'F'

		; Hex 0xA = dec 10
		; Ascii 'A' = dec 65
		; dec 55 = hex 0x37 ; So hex 0xA + 0x37 = 0x41 cause 10 + 55 = 65
		
	;; Mov ascii char to bx string
	move_intoBX:
		mov bx, hexString + 5 ; adding 5 will get you at the end of the hex string ; So base addr + len
		sub bx, cx ;sub loop counter interator ; Its 0 at the start
		mov [bx], al ; change it to number num in the ascii of the stirng
		ror dx, 4 ; rotate right by 4 bits ; 0x12AB -> 0xB12A -> 0xAB12 -> 0x2AB1 -> 0x12AB

		add cx, 1; inc the counter and inc the dec of bx :-> smarty smarty
		jmp hex_loop ; loop for dex digit in hex

	end_hexLoop:
		mov bx, hexString
		call print_str
		popa ; Pop them out
		ret

;; Data
hexString: db '0x0000', 0
